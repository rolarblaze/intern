from django.apps import AppConfig


class TrailwithitscrumyConfig(AppConfig):
    name = 'trailwithitscrumy'
